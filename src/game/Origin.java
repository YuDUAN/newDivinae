package game;

public enum Origin {
	daylight,darkness,nihility,
	dawn,dusk,
	NULL;

	//carte origin sont les 4 premier
	static public Origin rand(){
		Origin os[]=Origin.values();
		return os[(int)(Math.random()*(os.length-3))];
	}
	//origin de divinite sont le 1 2 4 5;
	static public Origin randGod(){
		Origin os[]=Origin.values();
		int r=(int)(Math.random()*(os.length-2));
		//rand entre 1--5 sauf que 3
		return os[r<2?r:(r+1)];
	}
}
