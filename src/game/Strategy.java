package game;

public interface Strategy {
	public abstract void run(Game game,Desk.Player me);
	public abstract boolean getWuxie(Game game,Desk.Player me,Desk.Player p,Card car);
}
