package main;
import java.lang.reflect.Constructor;
import java.util.Scanner;

import game.*;

public class Main {
	static public void main(String... args){
		Game game=new Game();
		System.out.println("hey!");
		//initiale carte
		game.initPile();
		Scanner in=new Scanner(System.in);
		System.out.println("Input the number of players:");
		int num=in.nextInt();
		System.out.println("Input the number of human players:");
		int human_num=in.nextInt();
		Strategy strategy[]=new Strategy[num];
		for(int i=0;i<num;i++){
			if(i<human_num)
				strategy[i]=new Game.HumanStrategy();
			else
				{
				double ran=Math.random();
				if(ran<1.0/2)
					{
					strategy[i]=new AI_1();
					System.out.println("player_AI_"+ (i+1) +" is a cute AI");
					}
				else 
					{
					strategy[i]=new AI_2();
					System.out.println("player_AI_"+ (i+1) +" is a super AI");
					}
				}
		}
		String names[]=new String[num];
		for(int i=0;i<num;i++)
			names[i]=(i<human_num?"Human_Player":"AI_Player")+"_"+(i+1);
		game.initPlayer(names,strategy);
		System.out.println("game start!");
	
		while(!game.checkEnd()){
			game.run();
		}
		System.out.println(game.desk.players.get(0).name+" win!");
	}
}
